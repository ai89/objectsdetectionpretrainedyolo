# ObjectsDetectionWithPretrainedYOLO

### Detecting objets with YOLO2 algorithm and the version 1.0.0-M1.1 of Deeplearning4j library.

This implementation shows how to use [Deeplearning4j](https://deeplearning4j.konduit.ai/) version 1.0.0-M1.1 to detect objects and people in an image file.

The detection is done using a pre-trained Yolo2 model that was trained on [COCO](https://cocodataset.org/#home) dataset. To ensure high precision detection, the images are preprocessed to fit Yolo2 requirements.
