package com.visiontotale;

import lombok.extern.slf4j.Slf4j;

import org.bytedeco.opencv.opencv_core.Mat;

import org.datavec.image.data.Image;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.transform.ColorConversionTransform;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.layers.objdetect.DetectedObject;
import org.deeplearning4j.nn.layers.objdetect.YoloUtils;
import org.deeplearning4j.util.ModelSerializer;
import org.deeplearning4j.zoo.ZooModel;
import org.deeplearning4j.zoo.model.YOLO2;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.nd4j.linalg.factory.Nd4j;

import java.io.File;
import java.util.List;

import static org.bytedeco.opencv.global.opencv_imgproc.COLOR_BGR2RGB;

@Slf4j
public class Detector {
    Params params = new Params();
    ZooModel yolo2Model = YOLO2
            .builder()
            .build();
    public void execute(String[] args) throws Exception {
        ComputationGraph model;
        String modelFilename = "Object_Detection_Model.zip";

        if (new File(modelFilename).exists()) {
            log.info("Loading existing model...");
            model = ModelSerializer.restoreComputationGraph(modelFilename);
        } else {
            log.info("Downloading the pretrained model...");
            model = (ComputationGraph) YOLO2.builder()
                    .build()
                    .initPretrained();
            if (params.isCanSave()) {
                ModelSerializer.writeModel(model, modelFilename, true);
            }
        }

        log.info(
                model.summary(
                        InputType.convolutional(
                                params.getHeight(),
                                params.getWidth(),
                                params.getNChannels()
                        )
                )
        );

        NativeImageLoader yoloImageLoader = new NativeImageLoader(
                params.getWidth(),
                params.getHeight(),
                params.getNChannels(),
                new ColorConversionTransform(COLOR_BGR2RGB));
        NativeImageLoader imageLoader = new NativeImageLoader();

        // Load the image from disk
        File imageFile = new File("Cars2.jpg");
        INDArray iNDArrayOriginalImage = imageLoader.asMatrix(imageFile);

        // Resize the image to match the required size by YOLO2
        Mat matResizedImage = yoloImageLoader.asMat(iNDArrayOriginalImage);

        // Scale the images, as in "normalize the pixels to be on the range from 0 to 1"
        ImagePreProcessingScaler scaler = new ImagePreProcessingScaler(0, 1);
        INDArray iNDArrayTransformedImage = yoloImageLoader.asMatrix(matResizedImage);
        scaler.transform(iNDArrayTransformedImage);

        // Perform the classification
        INDArray outputs = model.outputSingle(iNDArrayTransformedImage);
        List<DetectedObject> detectedObjects =
                YoloUtils.getPredictedObjects(
                    Nd4j.create(((YOLO2) yolo2Model).getPriorBoxes()),
                    outputs,
                    params.getDetectionThreshold(),
                    params.getNmsThreshold()
                );

        log.info(detectedObjects.size() + " objects detected.");

        // Annotate and draw bounding boxes on detected objects
        Image originalImage = imageLoader.asImageMatrix(imageFile);
        new Utils().annotateImagesAndDrawBoundingBoxes(originalImage.getOrigW(),
                originalImage.getOrigH(),
                matResizedImage,
                detectedObjects,
                "outputCars2.jpg");
    }
    public static void main(String[] args) throws Exception {
        // TODO Auto-generated method stub
        new Detector().execute(args);
    }
}
