package com.visiontotale;

import org.bytedeco.opencv.opencv_core.Mat;
import org.bytedeco.opencv.opencv_core.Point;
import org.bytedeco.opencv.opencv_core.Scalar;
import org.deeplearning4j.nn.layers.objdetect.DetectedObject;
import org.deeplearning4j.zoo.util.BaseLabels;
import org.deeplearning4j.zoo.util.darknet.COCOLabels;

import java.io.IOException;
import java.util.List;

import static org.bytedeco.opencv.global.opencv_imgcodecs.imwrite;
import static org.bytedeco.opencv.global.opencv_imgproc.FONT_HERSHEY_DUPLEX;
import static org.bytedeco.opencv.global.opencv_imgproc.putText;
import static org.bytedeco.opencv.global.opencv_imgproc.rectangle;

public class Utils {
    public void annotateImagesAndDrawBoundingBoxes(int imageWidth,
                                  int imageHeight,
                                  Mat rawImage,
                                  List<DetectedObject> detectedObjects,
                                  String outputImagePath) throws IOException {

        BaseLabels labels = new COCOLabels();
        Params params = new Params();

        for (DetectedObject detectedObject : detectedObjects) {
            double[] xy1 = detectedObject.getTopLeftXY();
            double[] xy2 = detectedObject.getBottomRightXY();
            String label = labels.getLabel(detectedObject.getPredictedClass());
            int x1 = (int) Math.round(imageWidth * xy1[0] / params.getGridWidth());
            int y1 = (int) Math.round(imageHeight * xy1[1] / params.getGridHeight());
            int x2 = (int) Math.round(imageWidth * xy2[0] / params.getGridWidth());
            int y2 = (int) Math.round(imageHeight * xy2[1] / params.getGridHeight());

            // Draw the rectangle
            rectangle(rawImage, new Point(x1, y1), new Point(x2, y2), Scalar.GREEN, 2, 0, 0);
            // Draw the label
            putText(rawImage, label, new Point(x1 + 2, y2 - 2), FONT_HERSHEY_DUPLEX, 1, Scalar.GREEN);
            // Store the file on disk
            imwrite(outputImagePath, rawImage);
        }
    }
}
