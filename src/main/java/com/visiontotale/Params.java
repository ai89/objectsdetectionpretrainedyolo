package com.visiontotale;

import lombok.Getter;

@Getter
public class Params {
    // Width required by the YOLO2 Model
    private int width = 416;

    private int nChannels = 3;

    // Height required by the YOLO2 Model
    private int height = 416;

    // Minimum confidence required to accept a prediction
    private double detectionThreshold = 0.5;

    // non-maximum suppression which removes redundant overlapping bounding boxes
    private double nmsThreshold = 0.4;

    private int gridWidth = 13;

    private int gridHeight = 13;

    private boolean canSave = true;
}
